using CockSizeBot;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddHealthChecks();
builder.Services.AddHostedService<BotService>();

var app = builder.Build();

app.MapHealthChecks("/health");

app.Run();