﻿using Telegram.Bot;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InlineQueryResults;

namespace CockSizeBot;

public class BotService: IHostedService
{
    private readonly TelegramBotClient _botClient;
    private readonly Random _random;

    public BotService(IConfiguration configuration)
    {
        _botClient = new TelegramBotClient(configuration["token"]);
        _random = new Random();
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        _botClient.StartReceiving(UpdateHandler, ErrorHandler, new ReceiverOptions
        {
            AllowedUpdates = new [] { UpdateType.InlineQuery },
            ThrowPendingUpdates = true
        }, cancellationToken);
        return Task.CompletedTask;
    }

    private Task UpdateHandler(ITelegramBotClient client, Update update, CancellationToken cancellationToken)
    {
        switch (update.Type)
        {
            case UpdateType.InlineQuery:
                var query = update.InlineQuery!;
                var sizeString = GetSize();
                return client.AnswerInlineQueryAsync(query.Id,
                    new InlineQueryResult[]
                        {
                            new InlineQueryResultArticle(Guid.NewGuid().ToString(), "Узнать размер 🍆", new InputTextMessageContent(sizeString)
                            {
                                ParseMode = ParseMode.Markdown
                            })
                        }, cacheTime:3600, isPersonal:true, cancellationToken:cancellationToken);
            case UpdateType.ChosenInlineResult:
                
                break;
        }
        return Task.CompletedTask;
    }

    private string GetSize()
    {
        var size = _random.Next(2, 40);
        var emoji = GetEmoji(size);
        return $"Мой размер члена *{size}см* {emoji}";
    }

    private static string GetEmoji(int size)
    {
        return size switch
        {
            >= 30 and < 40 => "😱",
            >= 20 and < 30 => "😎",
            >= 15 and < 20 => "😏",
            >= 10 and < 15 => "😐",
            >= 5 and < 10 => "🙁",
            _ => "😭"
        };
    }

    private static Task ErrorHandler(ITelegramBotClient _, Exception exception, CancellationToken arg3)
    {
        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}